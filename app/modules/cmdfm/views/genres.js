'use strict';

var Mn = require('backbone.marionette');

module.exports = Mn.CollectionView.extend({

  //class name
  className: 'genres-list__inner',

  // child view
  childView: Mn.ItemView.extend({
    events: {
      click: function() {
        this.model.trigger('genre:select', this.model);
      }
    },
    template: require('../templates/genres-item.jade')
  }),

  //init
  initialize: function(params) {
    var state = params.state;
    this.collection = params.collection;

    //look up to genres select
    this.collection.on('genre:select', function(model) {
      state.set('genre', model.get('slug'))
    });

  }

});
