'use strict';

var Mn = require('backbone.marionette');

module.exports = Mn.CollectionView.extend({
  className: 'tracks-list__inner',
  // child view
  childView: Mn.ItemView.extend({
    className: 'track-list__track',
    events: {
      click: function() {
        this.model.trigger('track:play', this.model);
      }
    },
    template: require('../templates/track-item.jade')
  }),

  //init
  initialize: function(params) {

    var state = params.state;
    this.collection = params.collection;

    // //look up to genres select
    this.collection.on('track:play', function(model) {
      state.set('play', model.toJSON());
    });

  }

});
