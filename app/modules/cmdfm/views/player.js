'use strict';

var Bb = require('backbone.marionette');

module.exports = Bb.View.extend({
  template: require('../templates/player.jade'),
  initialize: function(params) {
    this.model = params.state;
    this.model.on('change:play', this.play, this);
  },

  play: function(model) {
    var track = model.get('play');
    this.$el.html(this.template({
      stream_url: track.stream_url + '?client_id=d1cb6a77c196460fd629657d86458f36',
      image: track.artwork_url
    }));
  }
});
