'use strict'

//set up config
var C = {
  limit: 80
}

// Depends
var Bb = require('backbone');
var Mn = require('backbone.marionette');

//Collections
var GenresCollection  = require('./collections/genres');
var TracksCollection  = require('./collections/tracks');

// Views
var Player            = require('./views/player');
var GenresListView    = require('./views/genres');
var TracksListView    = require('./views/tracks');

module.exports = Mn.LayoutView.extend({

  state: new Bb.Model(C),

  className: 'cmdfm-player',
  template: require('./templates/layout.jade'),

  regions: {
    playerRegion: '.js-player',
    genresRegion: '.js-genres-list',
    tracksRegion: '.js-tracks-list'
  },

  initialize: function() {

    var genres = this.genres = new GenresCollection;
    var tracks = this.tracks = new TracksCollection;

    this.state.on('change:genre', function(model) {
      tracks.fetch({
        data: {
          genre: model.get('genre'),
          limit: model.get('limit')
        }
      })
    }, this);

    //fetch genres list
    this.genres.fetch()
  },

  onRender: function() {

    //player
    this.playerRegion.show(
      new Player({ state: this.state })
    );

    //genres list
    this.genresRegion.show(
      new GenresListView({ state: this.state, collection: this.genres })
    );

    //tracks list
    this.tracksRegion.show(
      new TracksListView({ state: this.state, collection: this.tracks })
    );

  }

});
