//God save the Dev
'use strict';

//Depends
var $ = require('jquery');

// Stylesheet entrypoint
require('_stylesheets/app.styl');

var CmdFm = require('./modules/cmdfm');

$('body').append(new CmdFm().render().$el);
